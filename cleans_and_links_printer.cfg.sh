#!/bin/bash
cp ~/printer_data/config/printer.cfg ~/klipperconfigurations/printer.cfg

rm ~/printer_data/config/printer*

ln -s ~/klipperconfigurations/printer.cfg ~/printer_data/config/printer.cfg
